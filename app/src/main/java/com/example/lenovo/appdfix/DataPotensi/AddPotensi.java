package com.example.lenovo.appdfix.DataPotensi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.appdfix.R;
import com.example.lenovo.appdfix.db.DBHandler;

public class AddPotensi extends AppCompatActivity {

    EditText namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;
    DBHandler dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_potensi);

        namadesa = (EditText) findViewById(R.id.namadesa);
        namapotensi = (EditText) findViewById(R.id.namapotensi);
        daerahpotensi = (EditText) findViewById(R.id.daerahpotensi);
        jenispotensi = (EditText) findViewById(R.id.jenispotensi);
        jumlahpotensi = (EditText) findViewById(R.id.jumlahpotensi);
//        namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;

        dbh = new DBHandler(this);


    }

    public void simpan(View view){

        ModelPotensi mp = new ModelPotensi();
        mp.setNamadesa(namadesa.getText().toString());
        mp.setNamapotensi(namapotensi.getText().toString());
        mp.setDaerahpotensi(daerahpotensi.getText().toString());
        mp.setJenispotensi(jenispotensi.getText().toString());
        mp.setJumlahpotensi(jumlahpotensi.getText().toString());
//        mp.setGambar(R.drawable.logo);

        dbh.AddPotensi(mp);

        Toast.makeText(getApplicationContext(), "Berhasil menambahkan data", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPotensi.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);//delay 5 detik


    }
}
