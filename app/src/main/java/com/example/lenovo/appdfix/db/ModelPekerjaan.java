package com.example.lenovo.appdfix.db;

public class ModelPekerjaan {

    private int id;
    private String nama;
    private String pekerjaan;
    private String alamat;

    public ModelPekerjaan() {
    }

    public ModelPekerjaan(int id, String nama, String pekerjaan, String alamat) {
        this.id = id;
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.alamat = alamat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
