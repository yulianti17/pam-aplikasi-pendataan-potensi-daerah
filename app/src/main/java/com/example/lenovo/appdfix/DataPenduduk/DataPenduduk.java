package com.example.lenovo.appdfix.DataPenduduk;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.lenovo.appdfix.R;
import com.example.lenovo.appdfix.db.DBHandler;

import java.util.ArrayList;

public class DataPenduduk extends AppCompatActivity {

    ArrayList<ModelPenduduk> data = new ArrayList<>();
    private static final String TAG = "DataPotensi";
    DBHandler dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_penduduk);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        dbh = new DBHandler(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DataPenduduk.this, AddPenduduk.class);
                startActivity(i);
            }
        });
        showData();
    }

    private void showData(){

        data = dbh.getAllRecordPenduduk();
        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerview");
        RecyclerView recyclerView = findViewById(R.id.recycler_data_penduduk);
        AdapterPenduduk adapter = new AdapterPenduduk(this,data);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

}
