package com.example.lenovo.appdfix.db;

public class ModelKerajinanDesa {

    private int id;
    private String nama_kerajinan;
    private String deskripsi;
    private String pembuat;

    public ModelKerajinanDesa() {
    }

    public ModelKerajinanDesa(int id, String nama_kerajinan, String deskripsi, String pembuat) {
        this.id = id;
        this.nama_kerajinan = nama_kerajinan;
        this.deskripsi = deskripsi;
        this.pembuat = pembuat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_kerajinan() {
        return nama_kerajinan;
    }

    public void setNama_kerajinan(String nama_kerajinan) {
        this.nama_kerajinan = nama_kerajinan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }
}
