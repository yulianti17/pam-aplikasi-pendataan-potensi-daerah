package com.example.lenovo.appdfix.db;

public class ModelDana {

    private int id;
    private long total_dana;
    private long jumlah_keluar;
    private String deskripsi_dana;

    public ModelDana(){

    }

    public ModelDana(int id, long total_dana, long jumlah_keluar, String deskripsi_dana) {
        this.id = id;
        this.total_dana = total_dana;
        this.jumlah_keluar = jumlah_keluar;
        this.deskripsi_dana = deskripsi_dana;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTotal_dana() {
        return total_dana;
    }

    public void setTotal_dana(long total_dana) {
        this.total_dana = total_dana;
    }

    public long getJumlah_keluar() {
        return jumlah_keluar;
    }

    public void setJumlah_keluar(long jumlah_keluar) {
        this.jumlah_keluar = jumlah_keluar;
    }

    public String getDeskripsi_dana() {
        return deskripsi_dana;
    }

    public void setDeskripsi_dana(String deskripsi_dana) {
        this.deskripsi_dana = deskripsi_dana;
    }
}
