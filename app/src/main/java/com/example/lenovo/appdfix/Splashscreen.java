package com.example.lenovo.appdfix;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.lenovo.appdfix.DataPenduduk.DataPenduduk;
//import com.example.lenovo.appdfix.DataPotensi.DataPotensi;

public class Splashscreen extends AppCompatActivity {
    private ImageView kue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        kue = findViewById(R.id.imageView);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        },3000);
        Animation myAnim = AnimationUtils.loadAnimation(this,R.anim.splashscreenanimation);
        kue.startAnimation(myAnim);
    }

}
