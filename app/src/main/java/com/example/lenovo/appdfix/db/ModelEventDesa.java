package com.example.lenovo.appdfix.db;

public class ModelEventDesa {

    private int id;
    private String judul_event;
    private String jenis_event;
    private String deskripsi_event;

    public ModelEventDesa() {
    }

    public ModelEventDesa(int id, String judul_event, String jenis_event, String deskripsi_event) {
        this.id = id;
        this.judul_event = judul_event;
        this.jenis_event = jenis_event;
        this.deskripsi_event = deskripsi_event;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul_event() {
        return judul_event;
    }

    public void setJudul_event(String judul_event) {
        this.judul_event = judul_event;
    }

    public String getJenis_event() {
        return jenis_event;
    }

    public void setJenis_event(String jenis_event) {
        this.jenis_event = jenis_event;
    }

    public String getDeskripsi_event() {
        return deskripsi_event;
    }

    public void setDeskripsi_event(String deskripsi_event) {
        this.deskripsi_event = deskripsi_event;
    }
}
