package com.example.lenovo.appdfix.db;

public class ModelDeatilAnggaran {

    private int id;
    private String jenis_anggaran;
    private long total_anggaran;
    private String deskripsi_anggaran;

    public ModelDeatilAnggaran() {
    }

    public ModelDeatilAnggaran(int id, String jenis_anggaran, long total_anggaran, String deskripsi_anggaran) {
        this.id = id;
        this.jenis_anggaran = jenis_anggaran;
        this.total_anggaran = total_anggaran;
        this.deskripsi_anggaran = deskripsi_anggaran;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis_anggaran() {
        return jenis_anggaran;
    }

    public void setJenis_anggaran(String jenis_anggaran) {
        this.jenis_anggaran = jenis_anggaran;
    }

    public long getTotal_anggaran() {
        return total_anggaran;
    }

    public void setTotal_anggaran(long total_anggaran) {
        this.total_anggaran = total_anggaran;
    }

    public String getDeskripsi_anggaran() {
        return deskripsi_anggaran;
    }

    public void setDeskripsi_anggaran(String deskripsi_anggaran) {
        this.deskripsi_anggaran = deskripsi_anggaran;
    }
}
