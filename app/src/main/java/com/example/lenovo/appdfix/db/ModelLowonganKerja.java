package com.example.lenovo.appdfix.db;

public class ModelLowonganKerja {

    private int id;
    private String jenis_loker;
    private long jumlah_dibituhkan;
    private String deskripsi_pekerjaan;

    public ModelLowonganKerja() {
    }

    public ModelLowonganKerja(int id, String jenis_loker, long jumlah_dibituhkan, String deskripsi_pekerjaan) {
        this.id = id;
        this.jenis_loker = jenis_loker;
        this.jumlah_dibituhkan = jumlah_dibituhkan;
        this.deskripsi_pekerjaan = deskripsi_pekerjaan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis_loker() {
        return jenis_loker;
    }

    public void setJenis_loker(String jenis_loker) {
        this.jenis_loker = jenis_loker;
    }

    public long getJumlah_dibituhkan() {
        return jumlah_dibituhkan;
    }

    public void setJumlah_dibituhkan(long jumlah_dibituhkan) {
        this.jumlah_dibituhkan = jumlah_dibituhkan;
    }

    public String getDeskripsi_pekerjaan() {
        return deskripsi_pekerjaan;
    }

    public void setDeskripsi_pekerjaan(String deskripsi_pekerjaan) {
        this.deskripsi_pekerjaan = deskripsi_pekerjaan;
    }
}
