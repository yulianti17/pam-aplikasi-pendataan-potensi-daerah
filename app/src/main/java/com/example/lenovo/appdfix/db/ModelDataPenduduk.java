package com.example.lenovo.appdfix.db;

public class ModelDataPenduduk {

    private int id;
    private String nama;
    private String alamat;
    private String nik;
    private String jenis_kelamin;
    private String nama_ayah;
    private String nama_ibu;
    private String status_perkawinan;
    private String golongan_darah;
    private String pekerjaan;

    public ModelDataPenduduk() {
    }

    public ModelDataPenduduk(int id, String nama, String alamat, String nik, String jenis_kelamin, String nama_ayah, String nama_ibu, String status_perkawinan, String golongan_darah, String pekerjaan) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.nik = nik;
        this.jenis_kelamin = jenis_kelamin;
        this.nama_ayah = nama_ayah;
        this.nama_ibu = nama_ibu;
        this.status_perkawinan = status_perkawinan;
        this.golongan_darah = golongan_darah;
        this.pekerjaan = pekerjaan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getNama_ayah() {
        return nama_ayah;
    }

    public void setNama_ayah(String nama_ayah) {
        this.nama_ayah = nama_ayah;
    }

    public String getNama_ibu() {
        return nama_ibu;
    }

    public void setNama_ibu(String nama_ibu) {
        this.nama_ibu = nama_ibu;
    }

    public String getStatus_perkawinan() {
        return status_perkawinan;
    }

    public void setStatus_perkawinan(String status_perkawinan) {
        this.status_perkawinan = status_perkawinan;
    }

    public String getGolongan_darah() {
        return golongan_darah;
    }

    public void setGolongan_darah(String golongan_darah) {
        this.golongan_darah = golongan_darah;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }
}
