package com.example.lenovo.appdfix.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.lenovo.appdfix.DataPenduduk.ModelPenduduk;
import com.example.lenovo.appdfix.DataPotensi.ModelPotensi;

import java.util.ArrayList;

public class DBHandler extends SQLiteOpenHelper {

    private final static String DB_NAME = "dbprofil";
    private final static int DB_VERSION = 1;

//    private static final String table_dana = "create table "
//            + "data_dana" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "total_dana" + " bigint not null, "
//            + "jumlah_keluar" + " bigint not null, "
//            + "deskipsi_dana"+ " varchar(1000) not null);";
//
//    private static final String table_detail_anggaran = "create table "
//            + "data_detailanggaran" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "jenis_anggaran" + " varchar(30), "
//            + "total_anggaran" + " bigint not null, "
//            + "deskipsi_anggaran"+ " varchar(1000) not null);";
//
//    private static final String table_event_desa = "create table "
//            + "data_eventdesa" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "judul_event" + " varchar(30), "
//            + "jenis_event" + " varchar(100), "
//            + "deskipsi_event"+ " varchar(1000) not null);";
//
//    private static final String table_hasil_tani = "create table "
//            + "data_hasiltani" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama_hasil_tani" + " varchar(30), "
//            + "jenis_tani" + " varchar(30), "
//            + "gambar" + " varchar(30), "
//            + "banyak_hasiltani" + " varchar(100));";
//
//    private static final String table_informasi_desa = "create table "
//            + "data_informasidesa" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "judul_informasi" + " varchar(30), "
//            + "deskripsi_informasi" + " varchar(100)); ";
//
//    private static final String table_kerajinan_desa = "create table "
//            + "data_kerajinandesan" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama_kerajinan" + " varchar(30), "
//            + "deskripsi" + " varchar(100), "
//            + "pembuat"+ " varchar(30) not null);";
//
//    private static final String table_lowongan_kerja = "create table "
//            + "data_lowongankerja" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "judul_loker" + " varchar(30), "
//            + "jenis_loker" + " varchar(30), "
//            + "jumlah_dibutuhkan" + " bigint not null, "
//            + "gambar" + " varchar(30), "
//            + "deskripsi_pekerjaan"+ " varchar(1000) not null);";
//
//    private static final String table_pariwisata = "create table "
//            + "data_pariwisata" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama_pariwisata" + " varchar(30), "
//            + "deskipsi_pariwisata"+ " varchar(1000) not null);";
//
//    private static final String table_pekerjaan = "create table "
//            + "data_pekerjaan" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama" + " varchar(30), "
//            + "pekerjaan" + " varchar(100), "
//            + "alamat"+ " varchar(100) not null);";
//
//    private static final String table_pendaftar_event = "create table "
//            + "data_pendaftarevent" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama_pendaftar" + " varchar(30), "
//            + "umur_pendaftar" + " integer, "
//            + "jenis_kelamin"+ " varchar(20) not null);";
//
    private static final String table_data_potensi= "create table "
            + "data_potensi" + "("
            + "id" +" integer primary key autoincrement, "
            + "namadesa" + " varchar(30), "
            + "namapotensi" + " varchar(100), "
            + "daerahpotensi" + " varchar(100), "
            + "jenispotensi" + " varchar(100), "
            + "jumlahpotensi" + " varchar(100));";

//    namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;

    private static final String table_data_penduduk = "create table "
            + "data_penduduk" + "("
            + "id" +" integer primary key autoincrement, "
            + "nama" + " varchar(30), "
            + "alamat" + " varchar(100), "
            + "nik"+ " varchar(50) not null, "
            + "jenis_kelamin" + " varchar(30), "
            + "nama_ayah" + " varchar(100), "
            + "nama_ibu" + " varchar(100), "
            + "status_perkawinan" + " varchar(100), "
            + "golongan_darah"+ " varchar(50) not null, "
            + "gambar" + " integer, "
            + "pekerjaan" + " varchar(100)); ";

//    private static final String table_data_user = "create table "
//            + "data_user" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nik" + " varchar(30), "
//            + "nama" + " varchar(100), "
//            + "username"+ " varchar(30) not null, "
//            + "password" + " varchar(30)) ";
//
//    private static final String table_data_wisata = "create table "
//            + "data_wisata" + "("
//            + "id" +" integer primary key autoincrement, "
//            + "nama_wisata" + " varchar(30), "
//            + "alamat_wisata" + " varchar(100), "
//            + "deskripsi_wisata"+ " varchar(1000), "
//            + "gambar" + " integer); ";

    public DBHandler (Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        db.execSQL(table_dana);
        db.execSQL(table_data_penduduk);
//        db.execSQL(table_data_user);
//        db.execSQL(table_data_wisata);
//        db.execSQL(table_detail_anggaran);
//        db.execSQL(table_event_desa);
//        db.execSQL(table_hasil_tani);
//        db.execSQL(table_informasi_desa);
//        db.execSQL(table_kerajinan_desa);
        db.execSQL(table_data_potensi);
//        db.execSQL(table_pariwisata);
//        db.execSQL(table_pekerjaan);
//        db.execSQL(table_pendaftar_event);
//        db.execSQL(table_pendidikan);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        onCreate(db);

    }


    ///////WIIISSSAAAATTTTAAAAAA//////////////

//    public ArrayList<ModelDataWisata> getAllRecordWisata(){
//        ArrayList<ModelDataWisata> motorList = new ArrayList<ModelDataWisata>();
//
//        String sql = "SELECT * FROM data_wisata";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelDataWisata userModels = new ModelDataWisata();
//                userModels.setId(Integer.parseInt(cursor.getString(0)));
//                userModels.setNama_wisata(cursor.getString(1));
//                userModels.setAlamat_wisata(cursor.getString(2));
//                userModels.setDeskripsi_wisata(cursor.getString(3));
//                userModels.setGambar(cursor.getInt(4));
//                motorList.add(userModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }
//
//    public void AddWisata(ModelDataWisata md){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("nama_wisata", md.getNama_wisata());
//        values.put("alamat_wisata", md.getAlamat_wisata());
//        values.put("deskripsi_wisata", md.getDeskripsi_wisata());
//        values.put("gambar", md.getGambar());
//
//        db.insert("data_wisata",null,values);
//        db.close();
//
//    }
//
//    public void EditWisata(ModelDataWisata md, int id){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("nama_wisata", md.getNama_wisata());
//        values.put("alamat_wisata", md.getAlamat_wisata());
//        values.put("deskripsi_wisata", md.getDeskripsi_wisata());
//        values.put("gambar", md.getGambar());
//
//        db.update("data_wisata", values,"id = "+id,null);
//        db.close();
//
//    }
//
//    public void DeleteWisata(int id){
//        SQLiteDatabase db = getWritableDatabase();
//        db.delete("data_wisata","id = "+id, null);
//    }
//
//
//    ////////PENDIIDIIKAANNNNN/////
//
//    public ArrayList<ModelDataPendidikan> getAllRecordPendidikan(){
//        ArrayList<ModelDataPendidikan> motorList = new ArrayList<ModelDataPendidikan>();
//
//        String sql = "SELECT * FROM data_pendidikan";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelDataPendidikan userModels = new ModelDataPendidikan();
//                userModels.setId(Integer.parseInt(cursor.getString(0)));
//                userModels.setTingkatan(cursor.getString(1));
//                userModels.setNama_sekolah(cursor.getString(2));
//                userModels.setGambar(cursor.getInt(3));
//                userModels.setDeskripsi_sekolah(cursor.getString(4));
//
//                motorList.add(userModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }
//
//    public void AddPendidikan(ModelDataPendidikan md){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("jenis_pendidikan", md.getTingkatan());
//        values.put("nama_sekolah", md.getNama_sekolah());
//        values.put("deskripsi_sekolah", md.getDeskripsi_sekolah());
//        values.put("gambar", md.getGambar());
//
//        System.out.println(md.getGambar());
//
//        db.insert("data_pendidikan",null,values);
//        db.close();
//
//    }
//
//    public void EditPendidikan(ModelDataPendidikan md, int id){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("jenis_pendidikan", md.getTingkatan());
//        values.put("nama_sekolah", md.getNama_sekolah());
//        values.put("deskripsi_sekolah", md.getDeskripsi_sekolah());
//        values.put("gambar", md.getGambar());
//
//        db.update("data_pendidikan", values,"id = "+id,null);
//        db.close();
//
//    }
//
//    public void DeletePendidikan(int id){
//        SQLiteDatabase db = getWritableDatabase();
//        db.delete("data_pendidikan","id = "+id, null);
//    }
//
//    ////////////////Hasillllll Taniiiii////////////
//
//    public ArrayList<ModelHasilTani> getAllRecordHasilTani(){
//        ArrayList<ModelHasilTani> motorList = new ArrayList<>();
//
//        String sql = "SELECT * FROM data_hasiltani";
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(sql,null);
//
//        if(cursor.moveToFirst()){
//            do{
//                ModelHasilTani userModels = new ModelHasilTani();
//                userModels.setId(Integer.parseInt(cursor.getString(0)));
//                userModels.setNama(cursor.getString(1));
//                userModels.setJenis(cursor.getString(2));
//                userModels.setGambar(cursor.getInt(3));
//                userModels.setBanyak(cursor.getString(4));
//
//                motorList.add(userModels);
//            }while(cursor.moveToNext());
//        }
//
//        cursor.close();
//
//        return motorList;
//    }
//
//    public void AddHasilTani(ModelHasilTani md){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("nama_hasil_tani", md.getNama());
//        values.put("jenis_tani", md.getJenis());
//        values.put("gambar", md.getGambar());
//        values.put("banyak_hasiltani", md.getBanyak());
//
//        System.out.println(md.getGambar());
//
//        db.insert("data_hasiltani",null,values);
//        db.close();
//
//    }
//
//    public void EditHasilTani(ModelHasilTani md, int id){
//
//        SQLiteDatabase db = getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("nama_hasil_tani", md.getNama());
//        values.put("jenis_tani", md.getJenis());
//        values.put("gambar", md.getGambar());
//        values.put("banyak_hasiltani", md.getBanyak());
//
//        db.update("data_hasiltani", values,"id = "+id,null);
//        db.close();
//
//    }
//
//    public void DeleteHasilTani(int id){
//        SQLiteDatabase db = getWritableDatabase();
//        db.delete("data_hasiltani","id = "+id, null);
//    }
//
//    //////////Lowongannnnn Kerjaaa////////

    public ArrayList<ModelPotensi> getAllRecordPotensi(){
        ArrayList<ModelPotensi> motorList = new ArrayList<>();

        String sql = "SELECT * FROM data_potensi";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql,null);

        if(cursor.moveToFirst()){
            do{
                ModelPotensi userModels = new ModelPotensi();
                userModels.setId(Integer.parseInt(cursor.getString(0)));
                userModels.setNamadesa(cursor.getString(1));
                userModels.setNamapotensi(cursor.getString(2));
                userModels.setDaerahpotensi(cursor.getString(3));
                userModels.setJenispotensi(cursor.getString(4));
                userModels.setJumlahpotensi(cursor.getString(5));

                motorList.add(userModels);
            }while(cursor.moveToNext());
        }



        cursor.close();

        return motorList;
    }

    public void AddPotensi(ModelPotensi md){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("namadesa", md.getNamadesa());
        values.put("namapotensi", md.getNamapotensi());
        values.put("daerahpotensi", md.getDaerahpotensi());
        values.put("jenispotensi", md.getJenispotensi());
        values.put("jumlahpotensi", md.getJumlahpotensi());

        db.insert("data_potensi",null,values);
        db.close();

    }

    public void EditPotensi(ModelPotensi md, int id){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("namadesa", md.getNamadesa());
        values.put("namapotensi", md.getNamapotensi());
        values.put("daerahpotensi", md.getDaerahpotensi());
        values.put("jenispotensi", md.getJenispotensi());
        values.put("jumlahpotensi", md.getJumlahpotensi());

        db.update("data_potensi", values,"id = "+id,null);
        db.close();

    }

    public void DeletePotensi(int id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("data_potensi","id = "+id, null);
    }

    //////////////////////PENDUDUDKKK////////////////
    public ArrayList<ModelPenduduk> getAllRecordPenduduk(){
        ArrayList<ModelPenduduk> motorList = new ArrayList<>();

        String sql = "SELECT * FROM data_penduduk";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql,null);

        if(cursor.moveToFirst()){
            do{
                ModelPenduduk userModels = new ModelPenduduk();
                userModels.setId(Integer.parseInt(cursor.getString(0)));
                userModels.setNama(cursor.getString(1));
                userModels.setAlamat(cursor.getString(2));
                userModels.setNik(cursor.getString(3));
                userModels.setJenis_kelamin(cursor.getString(4));
                userModels.setNama_ayah(cursor.getString(5));
                userModels.setNama_ibu(cursor.getString(6));
                userModels.setStatus_perkawinan(cursor.getString(7));
                userModels.setGolongan_darah(cursor.getString(8));
                userModels.setGambar(cursor.getInt(9));
                userModels.setPekerjaan(cursor.getString(10));

                motorList.add(userModels);
            }while(cursor.moveToNext());
        }



        cursor.close();

        return motorList;
    }

    public void AddPenduduk (ModelPenduduk md){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nama", md.getNama());
        values.put("alamat", md.getNama());
        values.put("nik", md.getNik());
        values.put("jenis_kelamin", md.getJenis_kelamin());
        values.put("nama_ayah",md.getNama_ayah());
        values.put("nama_ibu", md.getNama_ibu());
        values.put("status_perkawinan", md.getStatus_perkawinan());
        values.put("golongan_darah", md.getGolongan_darah());
        values.put("gambar", md.getGambar());
        values.put("pekerjaan", md.getPekerjaan());

        db.insert("data_penduduk",null,values);
        db.close();

    }

    public void EditPenduduk (ModelPenduduk md, int id){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nama", md.getNama());
        values.put("alamat", md.getNama());
        values.put("nik", md.getNik());
        values.put("jenis_kelamin", md.getJenis_kelamin());
        values.put("nama_ayah",md.getNama_ayah());
        values.put("nama_ibu", md.getNama_ibu());
        values.put("status_perkawinan", md.getStatus_perkawinan());
        values.put("golongan_darah", md.getGolongan_darah());
        values.put("gambar", md.getGambar());
        values.put("pekerjaan", md.getPekerjaan());

        db.update("data_penduduk", values,"id = "+id,null);
        db.close();

    }

    public void DeletePenduduk(int id){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("data_penduduk","id = "+id, null);
    }

}
