package com.example.lenovo.appdfix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.appdfix.api.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements View.OnClickListener {

    private EditText editEmail,editPassword,editNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        editNama = findViewById(R.id.editNama);

        findViewById(R.id.butLogin).setOnClickListener(this);
        findViewById(R.id.butSign).setOnClickListener(this);
    }

    private void userSign(){
        String email = editEmail.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String nama = editNama.getText().toString().trim();

        if(email.isEmpty()){
            editEmail.setError("Email is required");
            editEmail.requestFocus();
            return;
        }if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editEmail.setError("Enter a valid Email");
            editEmail.requestFocus();
            return;
        }if(password.isEmpty()){
            editPassword.setError("Password is required");
            editPassword.requestFocus();
            return;
        }if(password.length() < 6){
            editPassword.setError("Password should be 6 character    ");
            editPassword.requestFocus();
            return;
        }

        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .createUser(email,password,nama);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String s = null;

                try {
                    if(response.code() == 201) {
                        s = response.body().string();
                        Toast.makeText(Register.this, s, Toast.LENGTH_LONG).show();
                    }else{
                        s = response.errorBody().string();
                        Toast.makeText(Register.this, s, Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(s != null){
                    try{
                        JSONObject jsonObject = new JSONObject(s);
                        Toast.makeText(Register.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Register.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.butLogin:
                startActivity(new Intent(this,Login.class));
                break;
            case R.id.butSign:
                userSign();
                break;
        }
    }

}

