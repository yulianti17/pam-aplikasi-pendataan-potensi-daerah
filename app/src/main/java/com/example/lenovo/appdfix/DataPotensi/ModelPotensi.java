package com.example.lenovo.appdfix.DataPotensi;

public class ModelPotensi {

    private int id;
    private String namadesa;
    private String namapotensi;
    private String daerahpotensi;
    private String jenispotensi;
    private String jumlahpotensi;

    public ModelPotensi() {
    }

    public ModelPotensi(int id, String namadesa, String namapotensi, String daerahpotensi, String jenispotensi, String jumlahpotensi) {
        this.id = id;
        this.namadesa = namadesa;
        this.namapotensi = namapotensi;
        this.daerahpotensi = daerahpotensi;
        this.jenispotensi = jenispotensi;
        this.jumlahpotensi = jumlahpotensi;
//        namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNamadesa(String namadesa) {
        this.namadesa = namadesa;
    }

    public void setNamapotensi(String namapotensi) {
        this.namapotensi = namapotensi;
    }

    public void setDaerahpotensi(String daerahpotensi) {
        this.daerahpotensi = daerahpotensi;
    }

    public void setJenispotensi(String jenispotensi) {
        this.jenispotensi = jenispotensi;
    }

    public void setJumlahpotensi(String jumlahpotensi) {
        this.jumlahpotensi = jumlahpotensi;
    }

    public int getId() {
        return id;
    }

    public String getNamadesa() {
        return namadesa;
    }

    public String getNamapotensi() {
        return namapotensi;
    }

    public String getDaerahpotensi() {
        return daerahpotensi;
    }

    public String getJenispotensi() {
        return jenispotensi;
    }

    public String getJumlahpotensi() {
        return jumlahpotensi;
    }
}
