package com.example.lenovo.appdfix.DataPenduduk;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lenovo.appdfix.R;
import com.example.lenovo.appdfix.db.DBHandler;

public class DetailDataPenduduk extends AppCompatActivity {

    EditText nama, alamat, nik, jenis_kelamin, nama_ayah, nama_ibu, status_perkawinan, golongan_darah, pekerjaan;
    ImageView photo;
    DBHandler dbh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data_penduduk);
        dbh = new DBHandler(this);
        nama = (EditText)findViewById(R.id.nama_penduduknya);
        alamat = (EditText) findViewById(R.id.alamat_penduduknya);
        nik = (EditText)findViewById(R.id.nik_penduduknya);
        jenis_kelamin = (EditText)findViewById(R.id.jenis_kelaminnya);
        nama_ayah = (EditText)findViewById(R.id.nama_ayahnya);
        nama_ibu = (EditText)findViewById(R.id.nama_ibunya);
        status_perkawinan = (EditText)findViewById(R.id.status_perkawinannya);
        golongan_darah = (EditText)findViewById(R.id.golongan_darahnya);
        pekerjaan = (EditText)findViewById(R.id.pekerjaannya);
        photo = (ImageView)findViewById(R.id.image1);

        Intent i = getIntent();

        nama.setText(i.getStringExtra("nama"));
        alamat.setText(i.getStringExtra("alamat"));
        nik.setText(i.getStringExtra("nik"));
        jenis_kelamin.setText(i.getStringExtra("jenis_kelamin"));
        nama_ayah.setText(i.getStringExtra("nama_ayah"));
        nama_ibu.setText(i.getStringExtra("nama_ibu"));
        status_perkawinan.setText(i.getStringExtra("status_perkawinan"));
        golongan_darah.setText(i.getStringExtra("golongan_darah"));
        pekerjaan.setText(i.getStringExtra("pekerjaan"));
        photo.setImageResource(i.getIntExtra("gambar",0));


    }

    public void hapus(View view) {
        Intent i = getIntent();

        int id = i.getIntExtra("id", 0);
        dbh.DeletePenduduk(id);

        Toast.makeText(getApplicationContext(),"Data berhasil dihapus", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPenduduk.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);//delay 5 detik

    }

    public void edit(View view) {

        Intent i = getIntent();
        int id = i.getIntExtra("id", 0);
        ModelPenduduk md = new ModelPenduduk();
        md.setNama(nama.getText().toString());
        md.setAlamat(alamat.getText().toString());
        md.setNik(nik.getText().toString());
        md.setJenis_kelamin(jenis_kelamin.getText().toString());
        md.setNama_ayah(nama_ayah.getText().toString());
        md.setNama_ibu(nama_ibu.getText().toString());
        md.setStatus_perkawinan(status_perkawinan.getText().toString());
        md.setGolongan_darah(golongan_darah.getText().toString());
        md.setPekerjaan(pekerjaan.getText().toString());

        md.setGambar(R.drawable.logo);

        dbh.EditPenduduk(md,id);

        Toast.makeText(getApplicationContext(),"Data berhasil diubah", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPenduduk.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);//delay 5 detik

    }
}
