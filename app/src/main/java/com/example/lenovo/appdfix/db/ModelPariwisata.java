package com.example.lenovo.appdfix.db;

public class ModelPariwisata {

    private int id;
    private String nama_pariwisata;
    private String deskripsi_pariwisata;

    public ModelPariwisata() {
    }

    public ModelPariwisata(int id, String nama_pariwisata, String deskripsi_pariwisata) {
        this.id = id;
        this.nama_pariwisata = nama_pariwisata;
        this.deskripsi_pariwisata = deskripsi_pariwisata;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_pariwisata() {
        return nama_pariwisata;
    }

    public void setNama_pariwisata(String nama_pariwisata) {
        this.nama_pariwisata = nama_pariwisata;
    }

    public String getDeskripsi_pariwisata() {
        return deskripsi_pariwisata;
    }

    public void setDeskripsi_pariwisata(String deskripsi_pariwisata) {
        this.deskripsi_pariwisata = deskripsi_pariwisata;
    }
}
