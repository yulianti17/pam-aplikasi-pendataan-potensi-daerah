package com.example.lenovo.appdfix.DataPotensi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lenovo.appdfix.R;

import java.util.ArrayList;


public class AdapterPotensi extends RecyclerView.Adapter<AdapterPotensi.ViewHolder> {

    private static final String TAG = "AdapterPotensi";

    private ArrayList<ModelPotensi> data = new ArrayList<>();
    private Context mContext;

    public AdapterPotensi(Context mContext, ArrayList<ModelPotensi> data) {
        this.data = data;
        this.mContext = mContext;
    }

    public ArrayList<ModelPotensi> getData() {
        return data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_data_potensi, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final ModelPotensi p = getData().get(i);

        Log.d(TAG, "onBindViewHolder: called");

//        viewHolder.image.setImageResource(p.getGambar());
        viewHolder.namadesa.setText(p.getNamadesa());
        viewHolder.namapotensi.setText(p.getNamapotensi());



        viewHolder.rlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailDataPotensi.class);
                i.putExtra("id", p.getId());
                i.putExtra("namadesa", p.getNamadesa());
                i.putExtra("namapotensi", p.getNamapotensi());
                i.putExtra("daerahpotensi", p.getDaerahpotensi());
                i.putExtra("jenispotensi", p.getJenispotensi());
                i.putExtra("jumlahpotensi", p.getJumlahpotensi());

//                namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;
                mContext.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

//        ImageView image;
        TextView namadesa, namapotensi;
        RecyclerView rv;
        RelativeLayout rlayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

//            image = itemView.findViewById(R.id.image_penduduk);
            namadesa = itemView.findViewById(R.id.namadesa);
            namapotensi = itemView.findViewById(R.id.namapotensi);
//            rv = itemView.findViewById(R.id.recycler_data_wisata);
            rlayout = itemView.findViewById(R.id.rlayout);


        }
    }
}
