package com.example.lenovo.appdfix.db;

public class ModelPendaftarEvent {

    private int id;
    private String nama_pendaftar;
    private int umur_pendaftar;
    private String jenis_kelamin;

    public ModelPendaftarEvent() {
    }

    public ModelPendaftarEvent(int id, String nama_pendaftar, int umur_pendaftar, String jenis_kelamin) {
        this.id = id;
        this.nama_pendaftar = nama_pendaftar;
        this.umur_pendaftar = umur_pendaftar;
        this.jenis_kelamin = jenis_kelamin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_pendaftar() {
        return nama_pendaftar;
    }

    public void setNama_pendaftar(String nama_pendaftar) {
        this.nama_pendaftar = nama_pendaftar;
    }

    public int getUmur_pendaftar() {
        return umur_pendaftar;
    }

    public void setUmur_pendaftar(int umur_pendaftar) {
        this.umur_pendaftar = umur_pendaftar;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }
}
