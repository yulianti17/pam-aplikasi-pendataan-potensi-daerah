package com.example.lenovo.appdfix.db;

public class ModelInformasiDesa {

    private int id;
    private String judul_informasi;
    private String deskripsi_informasi;

    public ModelInformasiDesa() {
    }

    public ModelInformasiDesa(int id, String judul_informasi, String deskripsi_informasi) {
        this.id = id;
        this.judul_informasi = judul_informasi;
        this.deskripsi_informasi = deskripsi_informasi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul_informasi() {
        return judul_informasi;
    }

    public void setJudul_informasi(String judul_informasi) {
        this.judul_informasi = judul_informasi;
    }

    public String getDeskripsi_informasi() {
        return deskripsi_informasi;
    }

    public void setDeskripsi_informasi(String deskripsi_informasi) {
        this.deskripsi_informasi = deskripsi_informasi;
    }
}
