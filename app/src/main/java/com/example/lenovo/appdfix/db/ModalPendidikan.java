package com.example.lenovo.appdfix.db;

public class ModalPendidikan {

    private int id;
    private String jenis_pendidikan;
    private String nama_sekolah;
    private String deskripsi_sekolah;

    public ModalPendidikan() {
    }

    public ModalPendidikan(int id, String jenis_pendidikan, String nama_sekolah, String deskripsi_sekolah) {
        this.id = id;
        this.jenis_pendidikan = jenis_pendidikan;
        this.nama_sekolah = nama_sekolah;
        this.deskripsi_sekolah = deskripsi_sekolah;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis_pendidikan() {
        return jenis_pendidikan;
    }

    public void setJenis_pendidikan(String jenis_pendidikan) {
        this.jenis_pendidikan = jenis_pendidikan;
    }

    public String getNama_sekolah() {
        return nama_sekolah;
    }

    public void setNama_sekolah(String nama_sekolah) {
        this.nama_sekolah = nama_sekolah;
    }

    public String getDeskripsi_sekolah() {
        return deskripsi_sekolah;
    }

    public void setDeskripsi_sekolah(String deskripsi_sekolah) {
        this.deskripsi_sekolah = deskripsi_sekolah;
    }
}
