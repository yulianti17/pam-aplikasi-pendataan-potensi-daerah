package com.example.lenovo.appdfix.DataPotensi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.appdfix.R;
import com.example.lenovo.appdfix.db.DBHandler;

public class DetailDataPotensi extends AppCompatActivity {

    EditText namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;
    DBHandler dbh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data_potensi);
        dbh = new DBHandler(this);
        namadesa = (EditText)findViewById(R.id.namadesanya);
        namapotensi = (EditText) findViewById(R.id.namapotensinya);
        daerahpotensi = (EditText)findViewById(R.id.daerahpotensinya);
        jenispotensi = (EditText)findViewById(R.id.jenispotensinya);
        jumlahpotensi = (EditText)findViewById(R.id.jumlahpotensinya);

        Intent i = getIntent();

        namadesa.setText(i.getStringExtra("namadesa"));
        namapotensi.setText(i.getStringExtra("namapotensi"));
        daerahpotensi.setText(i.getStringExtra("daerahpotensi"));
        jenispotensi.setText(i.getStringExtra("jenispotensi"));
        jumlahpotensi.setText(i.getStringExtra("jumlahpotensi"));


    }

    public void hapus(View view) {
        Intent i = getIntent();

        int id = i.getIntExtra("id", 0);
        dbh.DeletePotensi(id);

        Toast.makeText(getApplicationContext(),"Data potensi berhasil dihapus", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPotensi.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);//delay 5 detik

    }

    public void edit(View view) {

        Intent i = getIntent();
        int id = i.getIntExtra("id", 0);
        ModelPotensi md = new ModelPotensi();
        md.setNamadesa(namadesa.getText().toString());
        md.setNamapotensi(namapotensi.getText().toString());
        md.setDaerahpotensi(daerahpotensi.getText().toString());
        md.setJenispotensi(jenispotensi.getText().toString());
        md.setJumlahpotensi(jumlahpotensi.getText().toString());
//        namadesa, namapotensi, daerahpotensi, jenispotensi, jumlahpotensi;
//        md.setGambar(R.drawable.logo);

        dbh.EditPotensi(md,id);

        Toast.makeText(getApplicationContext(),"Data potensi berhasil diubah ", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPotensi.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);

    }
}
