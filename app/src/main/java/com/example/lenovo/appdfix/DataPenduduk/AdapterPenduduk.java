package com.example.lenovo.appdfix.DataPenduduk;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lenovo.appdfix.R;

import java.util.ArrayList;


public class AdapterPenduduk extends RecyclerView.Adapter<AdapterPenduduk.ViewHolder> {

    private static final String TAG = "AdapterPotensi";

    private ArrayList<ModelPenduduk> data = new ArrayList<>();
    private Context mContext;

    public AdapterPenduduk(Context mContext, ArrayList<ModelPenduduk> data) {
        this.data = data;
        this.mContext = mContext;
    }

    public ArrayList<ModelPenduduk> getData() {
        return data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_data_penduduk, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final ModelPenduduk p = getData().get(i);

        Log.d(TAG, "onBindViewHolder: called");

        viewHolder.image.setImageResource(p.getGambar());
        viewHolder.nama.setText(p.getNama());
        viewHolder.nik_penduduk.setText(p.getNik());



        viewHolder.rlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailDataPenduduk.class);
                i.putExtra("id", p.getId());
                i.putExtra("nama", p.getNama());
                i.putExtra("alamat", p.getAlamat());
                i.putExtra("nik", p.getNik());
                i.putExtra("gambar", p.getGambar());
                i.putExtra("jenis_kelamin", p.getJenis_kelamin());
                i.putExtra("nama_ayah", p.getNama_ayah());
                i.putExtra("nama_ibu", p.getNama_ibu());
                i.putExtra("status_perkawinan", p.getStatus_perkawinan());
                i.putExtra("golongan_darah", p.getGolongan_darah());
                i.putExtra("pekerjaan", p.getPekerjaan());
                i.putExtra("gambar", p.getGambar());
                mContext.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView nama, nik_penduduk;
        RecyclerView rv;
        RelativeLayout rlayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image_penduduk);
            nama = itemView.findViewById(R.id.nama_penduduk);
            nik_penduduk = itemView.findViewById(R.id.nik_penduduk);
//            rv = itemView.findViewById(R.id.recycler_data_wisata);
            rlayout = itemView.findViewById(R.id.rlayout);


        }
    }
}
