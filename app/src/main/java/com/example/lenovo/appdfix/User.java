package com.example.lenovo.appdfix;

public class User {
    private int id;
    private String email,password,nama;

    public User(int id, String email, String password, String nama) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getNama() {
        return nama;
    }
}
