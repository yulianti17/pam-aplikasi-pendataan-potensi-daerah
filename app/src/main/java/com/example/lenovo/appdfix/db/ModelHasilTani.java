package com.example.lenovo.appdfix.db;

public class ModelHasilTani {

    private int id;
    private String jenis_tani;
    private String banyak_hasiltani;

    public ModelHasilTani() {
    }

    public ModelHasilTani(int id, String jenis_tani, String banyak_hasiltani) {
        this.id = id;
        this.jenis_tani = jenis_tani;
        this.banyak_hasiltani = banyak_hasiltani;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis_tani() {
        return jenis_tani;
    }

    public void setJenis_tani(String jenis_tani) {
        this.jenis_tani = jenis_tani;
    }

    public String getBanyak_hasiltani() {
        return banyak_hasiltani;
    }

    public void setBanyak_hasiltani(String banyak_hasiltani) {
        this.banyak_hasiltani = banyak_hasiltani;
    }
}
