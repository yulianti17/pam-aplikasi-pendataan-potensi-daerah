package com.example.lenovo.appdfix.DataPenduduk;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.appdfix.R;
import com.example.lenovo.appdfix.db.DBHandler;

public class AddPenduduk extends AppCompatActivity {

    EditText nama, alamat, nik, jenis_kelamin, nama_ayah, nama_ibu, status_perkawinan, golongan_darah, pekerjaan;
    DBHandler dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_penduduk);

        nama = (EditText) findViewById(R.id.nama_penduduk);
        alamat = (EditText) findViewById(R.id.alamat);
        nik = (EditText) findViewById(R.id.nik_penduduk);
        jenis_kelamin = (EditText) findViewById(R.id.jenis_kelamin);
        nama_ayah = (EditText) findViewById(R.id.nama_ayah);
        nama_ibu = (EditText) findViewById(R.id.nama_ibu);
        status_perkawinan = (EditText) findViewById(R.id.status_perkawinan);
        golongan_darah = (EditText) findViewById(R.id.golongan_darah);
        pekerjaan = (EditText) findViewById(R.id.pekerjaan);

        dbh = new DBHandler(this);


    }

    public void simpan(View view){

        ModelPenduduk mp = new ModelPenduduk();
        mp.setNama(nama.getText().toString());
        mp.setAlamat(alamat.getText().toString());
        mp.setNik(nik.getText().toString());
        mp.setJenis_kelamin(jenis_kelamin.getText().toString());
        mp.setNama_ayah(nama_ayah.getText().toString());
        mp.setNama_ibu(nama_ibu.getText().toString());
        mp.setStatus_perkawinan(status_perkawinan.getText().toString());
        mp.setGolongan_darah(golongan_darah.getText().toString());
        mp.setPekerjaan(pekerjaan.getText().toString());
        mp.setGambar(R.drawable.logo);

        dbh.AddPenduduk(mp);

        Toast.makeText(getApplicationContext(), "Berhasil menambah data", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                final Intent mainIntent = new Intent(getApplicationContext(),
                        DataPenduduk.class);
                startActivity(mainIntent);
                finish();
            }
        }, 200);//delay 5 detik


    }
}
